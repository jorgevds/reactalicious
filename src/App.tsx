import { Provider } from "react-redux";
import { store, persistor } from "./Redux/Store/store";
import DataWrapper from "./Components/App/DataWrapper";
import { PersistGate } from "redux-persist/integration/react";

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <DataWrapper />
      </PersistGate>
    </Provider>
  );
};

export default App;
