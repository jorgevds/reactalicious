import { Provider } from "react-redux";
import { store, persistor } from "./Redux/Store/store";
import { PersistGate } from "redux-persist/integration/react";
import { render, cleanup } from "@testing-library/react";
import DataWrapper from "./Components/App/DataWrapper";
import App from "./App";

afterEach(cleanup);

test("App renders", () => {
  render(<App />);
});

test("App renders with data", () => {
  const { queryByTestId } = render(
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <DataWrapper />
      </PersistGate>
    </Provider>
  );
  const app = queryByTestId("data");
  expect(app).toBeInTheDocument();
});

test("App store is not null", () => {
  render(<Provider store={store} />);

  expect(store).not.toBe(null);
});

test("App persistor is not null", () => {
  render(
    <Provider store={store}>
      <PersistGate persistor={persistor} />
    </Provider>
  );

  expect(persistor).not.toBe(null);
});
