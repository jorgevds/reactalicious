/* istanbul ignore file */
export const fetchUrl = {
  baseUrl: "https://api.punkapi.com/v2/beers?hops=cascade",
  randomUrl: "https://api.punkapi.com/v2/beers/random",
};
