/* istanbul ignore file */
import axios from "axios";
import { Beer } from "./interface";

export const fetchBaseList = async (
  dispatch: (arg0: { type: string; beer?: Beer[]; error?: any }) => void,
  loadingAction: string,
  successAction: string,
  failedAction: string,
  url: string
) => {
  try {
    dispatch({ type: loadingAction });
    const response = await axios.get(url);
    const result = await response.data.slice(0, 3); // Only fetch a couple for CRUD operations
    dispatch({ type: successAction, beer: result });
  } catch (error: any) {
    // Catch clause variable type annotation must be "any" or "unknown" if specified
    dispatch({ type: failedAction, error: error.message });
  }
};

export const fetchAnother = async (
  dispatch: (arg0: { type: string; beer?: Beer[]; error?: any }) => void,
  loadingAction: string,
  successAction: string,
  failedAction: string,
  url: string,
  state: Beer[]
) => {
  try {
    dispatch({ type: loadingAction });
    const response = await axios.get(url);
    const result = await response.data;
    dispatch({ type: successAction, beer: [...state, ...result] });
  } catch (error: any) {
    dispatch({ type: failedAction, error: error.message });
  }
};
