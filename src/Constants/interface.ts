/* istanbul ignore file */
export interface InitialState {
  loadingBeer: number;
  successBeer: number;
  failedBeer: number;
  beer: Beer[];
  error: string;
}

export interface Beer {
  id: number;
  srm: number;
  ph: number;
  image_url: string;
  name: string;
  tagline: string;
  description: string;
  brewers_tips: string;
  food_pairing: string[];
  first_brewed: string;
  ingredients: Ingredients;
  method: Method;
}

export interface Hops {
  name: string;
  amount: Amount;
  add: string;
  attribute: string;
}

export interface Malt {
  name: string;
  amount: Amount;
}

interface Amount {
  unit: string;
  value: number;
}

interface Ingredients {
  hops: Hops[];
  malt: Malt[];
  yeast: string;
}

interface Temp {
  value: number;
  unit: string;
}

export interface Mash {
  duration: number;
  temp: Temp;
}

interface Fermentation {
  temp: Temp;
}

interface Method {
  mash_temp: Mash[];
  fermentation: Fermentation;
}

export interface Navigation {
  id: number;
  name: string;
  href: string;
}

export interface BeerTypes {
  LOADING_BEER: string;
  SUCCESS_BEER: string;
  FAILED_BEER: string;
}
