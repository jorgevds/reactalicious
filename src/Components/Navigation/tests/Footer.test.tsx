import React from "react";
import { render, cleanup } from "@testing-library/react";
import Footer from "../Footer";
import { dummyNavigation } from "./Header.test";

afterEach(cleanup);

test("Component renders", () => {
  const { getByText } = render(<Footer navigation={dummyNavigation} />);

  const footer = getByText("test");
  expect(footer).toBeInTheDocument();
});
