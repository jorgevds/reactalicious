import React from "react";
import { render, cleanup } from "@testing-library/react";
import Header from "../Header";

afterEach(cleanup);

export const dummyNavigation = [
  {
    id: 1,
    name: "test",
    href: "",
  },
];

test("Component renders", () => {
  const { getByText } = render(<Header navigation={dummyNavigation} />);

  const header = getByText("test");
  expect(header).toBeInTheDocument();
});
