import React from "react";
import styles from "./Footer.module.css";
import { Navigation } from "../../Constants/interface";

interface FooterProps {
  navigation: Navigation[];
}

const Footer: React.FC<FooterProps> = ({ navigation }) => {
  return (
    <footer className={styles.navbar}>
      <div className={styles.flexBody}>
        <div className={styles.titleWrapper}>
          <h1 className={styles.title}>Reactalicious</h1>
        </div>
        <nav>
          <ul className={styles.navList}>
            {navigation.map((nav) => (
              <li key={nav.id}>
                <a href={nav.href}>{nav.name}</a>
              </li>
            ))}
          </ul>
        </nav>
      </div>
    </footer>
  );
};

export default Footer;
