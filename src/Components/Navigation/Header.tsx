import React from "react";
import styles from "./Header.module.css";
import { Navigation } from "../../Constants/interface";

interface HeaderProps {
  navigation: Navigation[];
}

const Header: React.FC<HeaderProps> = ({ navigation }) => {
  return (
    <header className={styles.navbar}>
      <nav>
        <ul className={styles.navList}>
          {navigation.map((nav) => (
            <li key={nav.id}>
              <a href={nav.href}>{nav.name}</a>
            </li>
          ))}
        </ul>
      </nav>
    </header>
  );
};

export default Header;
