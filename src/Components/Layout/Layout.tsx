import React from "react";
import Header from "../Navigation/Header";
import Footer from "../Navigation/Footer";
import { Navigation } from "../../Constants/interface";

const Layout: React.FC = ({ children }) => {
  const navigation: Navigation[] = [
    {
      id: 1,
      name: "Home",
      href: "/",
    },
    {
      id: 2,
      name: "Contact",
      href: "/contact",
    },
    {
      id: 3,
      name: "Whatever",
      href: "/whatever",
    },
  ];
  return (
    <>
      <Header navigation={navigation} />
      {children}
      <Footer navigation={navigation} />
    </>
  );
};

export default Layout;
