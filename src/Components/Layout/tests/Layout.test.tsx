import React from "react";
import { render, cleanup } from "@testing-library/react";
import Layout from "../Layout";

afterEach(cleanup);

test("Component renders", () => {
  const { getAllByText } = render(<Layout />);

  const layoutHeader = getAllByText(/Home/)[0];
  const layoutFooter = getAllByText(/Home/)[1];
  expect(layoutHeader).toBeInTheDocument();
  expect(layoutFooter).toBeInTheDocument();
});
