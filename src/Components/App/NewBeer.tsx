import React from "react";
import styles from "./NewBeer.module.css";
import question from "../../Assets/Images/QuestionMark.png";

export interface NewBeerProps {
  handleClick: () => void;
}

const NewBeer: React.FC<NewBeerProps> = ({ handleClick }) => {
  return (
    <article data-testid="newBeer">
      <div className={styles.imgWrapper}>
        <img
          src={question}
          className={styles.img}
          height="200px"
          alt="Mystery beer"
        />
      </div>
      <h1 className={styles.h1}>Mystery beer</h1>
      <p className={styles.tagline}>Your beer here?</p>

      <button onClick={handleClick}>Click me</button>
    </article>
  );
};

export default NewBeer;
