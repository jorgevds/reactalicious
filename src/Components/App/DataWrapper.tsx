import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { InitialState } from "../../Constants/interface";
import { RootState } from "../../Redux/reducers/rootReducer";
import { beerActions } from "../../Redux/Actions/beerActions";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import BeerDetail from "./BeerDetail";
import BeerOverview from "./BeerOverview";

const DataWrapper: React.FC = ({}) => {
  const dispatch = useDispatch();
  const beerList: InitialState = useSelector(
    (state: RootState) => state.beerReducer
  );

  useEffect(() => {
    if (beerList.beer.length === 0) {
      dispatch(beerActions.getBeerList());
    }
  }, [dispatch, beerList]);

  const handleClick = () => {
    dispatch(beerActions.getAnotherBeer());
  };

  return (
    <>
      {beerList ? (
        <div data-testid="data">
          <Router>
            <Switch>
              <Route exact={true} path="/">
                <h1 style={{ textAlign: "center" }}>Reactalicious</h1>
                <BeerOverview
                  beerList={beerList.beer}
                  handleClick={handleClick}
                />
              </Route>
              <Route path="/:beerId">
                <BeerDetail beerList={beerList.beer} />
              </Route>
            </Switch>
          </Router>
        </div>
      ) : (
        <>
          <h1>Loading... Please hold</h1>
        </>
      )}
    </>
  );
};

export default DataWrapper;
