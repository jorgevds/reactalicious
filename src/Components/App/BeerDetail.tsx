import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import styles from "./BeerDetail.module.css";
import { Beer, Hops, Malt, Mash } from "../../Constants/interface";

interface BeerDetailProps {
  beerList: Beer[];
}

const BeerDetail: React.FC<BeerDetailProps> = ({ beerList }) => {
  let { beerId } = useParams<{ beerId: string | undefined }>();
  const [beer, setBeer] = useState<Beer[] | null>();

  useEffect(() => {
    const beerFiltered =
      beerList.length > 0
        ? beerList.filter((beer: Beer) => beer.id.toString() === beerId)
        : null;
    setBeer(beerFiltered);
  }, [beerList, beerId]);

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <section className={styles.container} data-testid="beerDetail">
      {beer != null &&
        beer.map((b: Beer) => (
          <div key={b.srm} className={styles.flex}>
            <h1>{b.name}</h1>
            <div className={styles.imgWrapper}>
              <img
                src={b.image_url}
                className={styles.img}
                height="350px"
                alt={b.name}
              />
            </div>
            <p className={styles.tagline}>{b.tagline}</p>
            <p>{b.description}</p>

            <h2>Goes well with:</h2>

            <div>
              {b.food_pairing.map((food: string, i: number) => (
                <p key={`food: ${i}`} className={styles.pairing}>
                  {food}
                </p>
              ))}
            </div>
            <p className={styles.brewer}>Brewer's tip: {b.brewers_tips}</p>
            <h3 className={styles.ingredientList}>Ingredients</h3>
            <h4 className={styles.ingredientType}>Hops</h4>
            {b.ingredients.hops.map((hops: Hops, i: number) => (
              <div key={`hops: ${i}`}>
                <p className={styles.ingredientName}>{hops.name}</p>
                <aside>
                  amount: {hops.amount.value} {hops.amount.unit}
                </aside>
                <aside> added: {hops.add}</aside>
                <aside> attribute: {hops.attribute}</aside>
              </div>
            ))}
            <h4 className={styles.ingredientType}>Malt</h4>
            {b.ingredients.malt.map((malt: Malt, i: number) => (
              <div key={`malt: ${i}`}>
                <p className={styles.ingredientName}>{malt.name}</p>
                <aside>
                  amount: {malt.amount.value} {malt.amount.unit}
                </aside>
              </div>
            ))}
            <h4 className={styles.ingredientType}>Yeast</h4>
            <p className={styles.ingredientName}>{b.ingredients.yeast}</p>
            <h3>Method</h3>
            {b.method.mash_temp.map((mash: Mash) => (
              <div key={mash.duration}>
                <p>
                  Mashed at {mash.temp.value}° {mash.temp.unit} for{" "}
                  {mash.duration} minutes
                </p>
              </div>
            ))}
            <p>
              Fermented at {b.method.fermentation.temp.value}°{" "}
              {b.method.fermentation.temp.unit}
            </p>
            <h3>Further details</h3>
            <aside>first brewed: {b.first_brewed}</aside>
            <aside>ph: {b.ph}</aside>
          </div>
        ))}
      <Link to="/">
        <button
          className={styles.button}
          onClick={scrollToTop}
          data-testid="backButton"
        >
          Back to overview
        </button>
      </Link>
    </section>
  );
};

export default BeerDetail;
