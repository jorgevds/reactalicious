import React from "react";
import { Link } from "react-router-dom";
import styles from "./BeerOverview.module.css";
import NewBeer from "./NewBeer";
import { Beer } from "../../Constants/interface";

export interface BeerOverviewProps {
  beerList: Beer[];
  handleClick: () => void;
}

const BeerOverview: React.FC<BeerOverviewProps> = ({
  beerList,
  handleClick,
}) => {
  return (
    <section data-testid="overview">
      <ul className={styles.flex}>
        {beerList.length > 0
          ? beerList.map((beer: Beer) => (
              <li key={beer.id} className={styles.card}>
                <article>
                  <div className={styles.imgWrapper}>
                    <img
                      src={beer.image_url}
                      className={styles.img}
                      height="250px"
                      alt={beer.name}
                    />
                  </div>
                  <h1 className={styles.h1}>{beer.name}</h1>
                  <p className={styles.tagline}>{beer.tagline}</p>
                  <p className={styles.truncate}>{beer.description}</p>
                  <Link to={`/${beer.id}`}>
                    <button>Read more</button>
                  </Link>
                </article>
              </li>
            ))
          : null}
        <li className={styles.card} data-testid="newBeerLI">
          <NewBeer handleClick={handleClick} />
        </li>
      </ul>
    </section>
  );
};

export default BeerOverview;
