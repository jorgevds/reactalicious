import React from "react";
import { render, cleanup } from "@testing-library/react";
import DataWrapper from "../DataWrapper";
import { Provider } from "react-redux";
import { InitialState } from "../../../Constants/interface";
import { store } from "../../../Redux/Store/store";
import BeerOverview from "../BeerOverview";
import { Router, Route, Switch } from "react-router-dom";
import BeerDetail from "../BeerDetail";
import { createMemoryHistory } from "history";

afterEach(cleanup);

export const dummyClick = () => {
  console.log("I'm a dummy fn");
};

export const dummyBeerList: InitialState = {
  loadingBeer: 0,
  successBeer: 0,
  error: "",
  failedBeer: 0,
  beer: [
    {
      id: 1,
      srm: 10,
      ph: 4.4,
      image_url: "https://images.punkapi.com/v2/keg.png",
      name: "beer",
      tagline: "A Real Bitter Experience.",
      description:
        "A light, crisp and bitter IPA brewed with English and American hops. A small batch brewed only once.",
      brewers_tips:
        "The earthy and floral aromas from the hops can be overpowering. Drop a little Cascade in at the end of the boil to lift the profile with a bit of citrus.",
      food_pairing: ["Spicy chicken massala"],
      first_brewed: "09/2007",
      ingredients: {
        hops: [
          {
            name: "Fuggles",
            amount: { unit: "grams", value: 25 },
            add: "start",
            attribute: "bitter",
          },
        ],
        malt: [{ name: "Caramalt", amount: { unit: "kilograms", value: 0.2 } }],
        yeast: "Wyeast 1056",
      },
      method: {
        mash_temp: [
          {
            duration: 75,
            temp: {
              value: 64,
              unit: "celsius",
            },
          },
        ],
        fermentation: { temp: { value: 19, unit: "celsius" } },
      },
    },
  ],
};

const history = createMemoryHistory();

test("Component renders", () => {
  const { queryByTestId } = render(
    <Provider store={store}>
      <DataWrapper />
    </Provider>
  );

  const dataWrapper = queryByTestId("data");
  expect(dataWrapper).toBeInTheDocument();
}); // So this one only took 2 whole hours lmao

test("Fetch function returns data", () => {
  const { queryByText } = render(
    <Provider store={store}>
      <DataWrapper />
    </Provider>
  );

  const reactalicious = queryByText(/reactalicious/i);
  expect(reactalicious).toBeInTheDocument();

  const loadingScreen = queryByText(/loading/i);
  expect(loadingScreen).not.toBeInTheDocument();
});

test("Router renders the home page correctly", () => {
  const { queryByTestId } = render(
    <Router history={history}>
      <BeerOverview beerList={dummyBeerList.beer} handleClick={dummyClick} />
    </Router>
  );

  const backButton = queryByTestId("backButton");
  expect(backButton).not.toBeInTheDocument();
});

test("Router renders the detail page correctly", () => {
  history.push("/1");

  const { queryByTestId } = render(
    <Router history={history}>
      <Switch>
        <Route path="/:beerId">
          <BeerDetail beerList={dummyBeerList.beer} />
        </Route>
      </Switch>
    </Router>
  );

  const backButton = queryByTestId("backButton");
  expect(backButton).toBeInTheDocument();
});
