import React from "react";
import { render, cleanup, fireEvent } from "@testing-library/react";
import { dummyBeerList } from "./DataWrapper.test";
import { BrowserRouter as Router } from "react-router-dom";
import BeerDetail from "../BeerDetail";

afterEach(cleanup);

window.scrollTo = jest.fn();

test("Component renders", () => {
  const { getByTestId } = render(
    <Router>
      <BeerDetail beerList={dummyBeerList.beer} />
    </Router>
  );

  const component = getByTestId("beerDetail");
  expect(component).toBeInTheDocument();
});

test("Prop beerList is not null", () => {
  render(
    <Router>
      <BeerDetail beerList={dummyBeerList.beer} />
    </Router>
  );

  expect(dummyBeerList).not.toBe(null);
});

test("ScrollToTop fn works", () => {
  const { getByText } = render(
    <Router>
      <BeerDetail beerList={dummyBeerList.beer} />
    </Router>
  );

  fireEvent.click(getByText(/back to overview/i));
  expect(window.scrollTo).toHaveBeenCalledTimes(1);
});
