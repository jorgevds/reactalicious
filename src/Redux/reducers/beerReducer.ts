import { InitialState } from "../../Constants/interface";
import produce from "immer";
import { beerTypes } from "../Actions/beerActions";
import { AnyAction } from "redux";

export const initialState: InitialState = {
  loadingBeer: 0,
  successBeer: 0,
  failedBeer: 0,
  beer: [],
  error: "",
};

export const beerReducer = (
  previousState = initialState,
  action: AnyAction
) => {
  return produce(previousState, (newState) => {
    // This fucker was causing all my tests to fail, and I had no clue what to do. Adding the if check solved the problem
    if (beerTypes) {
      switch (action.type) {
        case beerTypes.LOADING_BEER: {
          newState.loadingBeer = 1;
          newState.successBeer = initialState.successBeer;
          newState.beer = initialState.beer;
          newState.failedBeer = initialState.failedBeer;
          return newState;
        }
        case beerTypes.SUCCESS_BEER: {
          newState.loadingBeer = 0;
          newState.successBeer = 1;
          newState.beer = action.beer;
          return newState;
        }
        case beerTypes.FAILED_BEER: {
          newState.loadingBeer = 0;
          newState.failedBeer = 1;
          newState.error = action.error;
          return newState;
        }
        default: {
          return previousState;
        }
      }
    }
  });
};
