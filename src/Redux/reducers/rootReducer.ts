import { beerReducer } from "./beerReducer";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
  beerReducer: beerReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;
