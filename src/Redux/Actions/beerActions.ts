import { AppDispatch, store } from "../Store/store";
import { fetchBaseList, fetchAnother } from "../../Constants/fetch";
import { fetchUrl } from "../../Constants/fetchUrl";
import { BeerTypes } from "../../Constants/interface";

export const beerTypes: BeerTypes = {
  LOADING_BEER: "LOADING_BEER",
  SUCCESS_BEER: "SUCCESS_BEER",
  FAILED_BEER: "FAILED_BEER",
};

export const beerActions = {
  getBeerList: () => {
    return (dispatch: AppDispatch) => {
      return fetchBaseList(
        dispatch,
        beerTypes.LOADING_BEER,
        beerTypes.SUCCESS_BEER,
        beerTypes.FAILED_BEER,
        fetchUrl.baseUrl
      );
    };
  },
  getAnotherBeer: () => {
    return (dispatch: AppDispatch) => {
      return fetchAnother(
        dispatch,
        beerTypes.LOADING_BEER,
        beerTypes.SUCCESS_BEER,
        beerTypes.FAILED_BEER,
        fetchUrl.randomUrl,
        store.getState().beerReducer.beer
      );
    };
  },
};
